import { Checkbox, FormControlLabel } from "@mui/material";

type CCheckboxProps = {
  label?: string;
  size?: "large" | "small" | "medium";
  onChange?: (...event: any[]) => void;
  value?: boolean;
};
const CCheckbox = (props: CCheckboxProps) => {
  const { label, size } = props;
  return (
    <FormControlLabel
      {...props}
      control={<Checkbox size={size} />}
      label={label}
    />
  );
};

export default CCheckbox;
