import { Button, ButtonProps, styled } from "@mui/material";
import React from "react";

type CButtonProps = {
  startIcon?: React.ReactNode;
  endIcon?: React.ReactNode;
  children: React.ReactNode | string;
  variant?: "outlined" | "contained";
  type?: "button" | "submit" | "reset";
  onClick?: (...args: any[]) => void;
  style?: any;
};

const MuiButton = styled(Button)<ButtonProps>(() => ({
  textTransform: "unset",
}));

const CButton = (props: CButtonProps) => {
  const { children, endIcon, startIcon, variant, type, onClick } = props;
  return (
    <MuiButton
      {...props}
      variant={variant}
      startIcon={startIcon ?? ""}
      endIcon={endIcon ?? ""}
      onClick={onClick}
      type={type}
    >
      {children}
    </MuiButton>
  );
};

export default CButton;
