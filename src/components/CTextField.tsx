import { TextField, TextFieldProps, styled } from "@mui/material";

type CTextFieldProps = {
  label?: string;
  variant?: "standard" | "filled" | "outlined";
  name?: string;
  register?: any;
  error?: boolean;
  helperText?: string;
  onChange?: (...event: any[]) => void;
  InputProps?: any;
  value?: string;
  type?: "text" | "password";
};

const MuiTextField = styled(TextField)<TextFieldProps>(() => ({
  width: "100%",
  ".MuiInputBase-input": {
    fontSize: "14px",
    padding: "12px 14px",
  },
  ".MuiInputLabel-root": {
    fontSize: "14px",
    transform: "translate(14px, 13px)",
  },
  ".Mui-focused.MuiInputLabel-root": {
    transform: "translate(14px, -9px) scale(0.75)",
  },
  ".MuiInputLabel-shrink": {
    transform: "translate(14px, -9px) scale(0.75) !important",
  },
}));

export const CTextField = (props: CTextFieldProps) => {
  const { label, variant, name, register, error = false, helperText } = props;
  return (
    <MuiTextField
      {...register}
      {...props}
      id="standard-basic"
      label={label}
      variant={variant}
      name={name}
      error={error}
      helperText={error && helperText ? helperText : ""}
    />
  );
};
