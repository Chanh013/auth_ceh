import Logistics1 from "../assets/imgs/Logistics1.svg";
import Logistics2 from "../assets/imgs/Logistics2.svg";
import Logistics3 from "../assets/imgs/Logistics3.svg";
import Logistics4 from "../assets/imgs/Logistics4.svg";
import Logistics5 from "../assets/imgs/Logistics5.svg";
import DashboardIcon from "@mui/icons-material/Dashboard";
import InsightsIcon from "@mui/icons-material/Insights";
import InfoIcon from '@mui/icons-material/Info';
import PermContactCalendarIcon from '@mui/icons-material/PermContactCalendar';
import NewspaperIcon from '@mui/icons-material/Newspaper';
import QrCodeIcon from '@mui/icons-material/QrCode';

export const listIntro = [
  { img: Logistics1 },
  { img: Logistics2 },
  { img: Logistics3 },
  { img: Logistics4 },
  { img: Logistics5 },
];

export const listMenu = [
  { title: "Dashboard", icon: DashboardIcon },
  { title: "Introduce", icon: InsightsIcon },
  { title: "Product", icon: QrCodeIcon },
  { title: "News", icon: NewspaperIcon },
  { title: "Contact", icon: PermContactCalendarIcon },
  { title: "About", icon: InfoIcon },
];
