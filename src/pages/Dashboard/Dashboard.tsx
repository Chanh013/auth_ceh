import { Grid, Stack, Typography } from "@mui/material";
import CDrawer from "../../components/CDrawer";
import CButton from "../../components/CButton";
import Home from "../../assets/imgs/Home.svg";
import CRadialBarChart from "../../components/CRadialBarChart";
import CApexChart from "../../components/CApexChart";

const Dashboard = () => {
  return (
    <CDrawer title="Dashboard">
      <Grid container gap={2}>
        <Grid
          item
          xs={12}
          md={8}
          style={{
            padding: "20px 40px",
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            background: "rgb(30 93 242)",
            borderRadius: "16px",
          }}
        >
          <Grid
            container
            spacing={2}
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              boxShadow: "rgba(149, 157, 165, 0.2) 0px 8px 24px",
            }}
          >
            <Grid item xs={6}>
              <Stack gap={1}>
                <Typography
                  variant="inherit"
                  maxWidth={300}
                  fontSize={22}
                  fontWeight={600}
                  color="#fcfdff"
                >
                  Welcome back 👋 Nguyen Minh Chanh
                </Typography>
                <Typography variant="caption" color="#c5cad6">
                  If you are going to use a passage of Lorem Ipsum, you need to
                  be sure there isn't anything.
                </Typography>
              </Stack>
              <CButton style={{ marginTop: "10px" }} variant="contained">
                Go Now
              </CButton>
            </Grid>
            <Grid item xs={6}>
              <img src={Home} alt="" />
            </Grid>
          </Grid>
        </Grid>
        <Grid
          item
          xs={12}
          md={3.8}
          style={{
            width: "100%",
            padding: "0px",
          }}
        >
          <CRadialBarChart />
        </Grid>
      </Grid>
      <Grid style={{ width: "100%", marginTop: "10px" }}>
        <CApexChart />
      </Grid>
    </CDrawer>
  );
};

export default Dashboard;
