import { Grid, Paper } from "@mui/material";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import SwiperIntro from "./components/SwiperIntro";
import LoginForm from "./components/LoginForm";

const defaultTheme = createTheme();

const Login = () => {
  return (
    <ThemeProvider theme={defaultTheme}>
      <Grid container component="main" sx={{ height: "100vh" }}>
        <Grid item xs={false} sm={5} md={6}>
          <SwiperIntro />
        </Grid>
        <Grid item xs={12} sm={7} md={6} component={Paper} elevation={6} square>
          <LoginForm />
        </Grid>
      </Grid>
    </ThemeProvider>
  );
};

export default Login;
