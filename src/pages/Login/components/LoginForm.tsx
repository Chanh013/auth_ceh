import {
  Box,
  FormControl,
  InputAdornment,
  Link,
  Stack,
  Typography,
} from "@mui/material";
import { CTextField } from "../../../components/CTextField";
import { AccountCircle, Visibility, VisibilityOff } from "@mui/icons-material";
import { useState } from "react";
import CCheckbox from "../../../components/CCheckbox";
import CButton from "../../../components/CButton";
import { useForm, Controller } from "react-hook-form";
import { useNavigate } from "react-router-dom";

interface IFormInputLogin {
  account: string;
  password: string;
  remember: boolean;
}

const LoginForm = () => {
  const [visitPass, setVisitPass] = useState(false);
  const navigate = useNavigate();

  const { control, handleSubmit } = useForm({
    defaultValues: {
      account: "",
      password: "",
      remember: false,
    },
  });

  const handleLoginForm = (value: IFormInputLogin) => {
    console.log(value);
    navigate("/")
  };
  return (
    <Box
      sx={{
        my: 8,
        mx: 4,
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
      }}
    >
      <Stack
        direction="column"
        justifyContent="center"
        alignItems="center"
        spacing={7}
      >
        <img
          src="http://cehsoft.com/wp-content/themes/cehsoft/images/logo-1.png"
          alt=""
          className="logo-main"
        />
        <Box
          sx={{
            display: "flex",
            flexDirection: "column",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <Typography variant="h5" fontSize={28} fontWeight={500}>
            Hello Again!
          </Typography>
          <Typography
            variant="caption"
            maxWidth={450}
            textAlign="center"
            color="#8d8787"
          >
            Please <span style={{ fontWeight: "bolder" }}>Log-in</span> to
            experience and use the convenient services we provide to the fullest
          </Typography>
          <form
            style={{
              marginTop: "20px",
              width: "100%",
              display: "flex",
              flexDirection: "column",
              gap: "12px",
            }}
            onSubmit={handleSubmit(handleLoginForm)}
          >
            <FormControl>
              <Controller
                control={control}
                name="account"
                rules={{
                  required: true,
                }}
                render={({
                  field: { onChange, value },
                  fieldState: { error },
                }) => (
                  <CTextField
                    onChange={onChange}
                    error={!!error}
                    value={value}
                    label="Enter Account"
                    helperText="Account is required"
                    InputProps={{
                      endAdornment: (
                        <InputAdornment position="start">
                          <AccountCircle />
                        </InputAdornment>
                      ),
                    }}
                  />
                )}
              />
            </FormControl>
            <FormControl>
              <Controller
                control={control}
                name="password"
                rules={{
                  required: true,
                }}
                render={({
                  field: { onChange, value },
                  fieldState: { error },
                }) => (
                  <CTextField
                    onChange={onChange}
                    error={!!error}
                    value={value}
                    label="Enter Password"
                    type={visitPass ? "text" : "password"}
                    helperText="Password is required"
                    InputProps={{
                      endAdornment: (
                        <InputAdornment
                          onClick={() => setVisitPass(!visitPass)}
                          position="start"
                          sx={{ cursor: "pointer" }}
                        >
                          {!visitPass ? <Visibility /> : <VisibilityOff />}
                        </InputAdornment>
                      ),
                    }}
                  />
                )}
              />
            </FormControl>
            <Stack
              display="flex"
              flexDirection="row"
              justifyContent="space-between"
              alignItems="center"
              marginTop={-2}
            >
              <FormControl>
                <Controller
                  control={control}
                  name="remember"
                  render={({ field: { onChange, value } }) => (
                    <CCheckbox
                      onChange={onChange}
                      value={value}
                      size="small"
                      label="Remember Me"
                    />
                  )}
                />
              </FormControl>
              <Link fontSize={12} href="#">
                Forgot Password
              </Link>
            </Stack>
            <CButton
              type="submit"
              style={{ height: "42px" }}
              variant="contained"
            >
              Login
            </CButton>
          </form>
        </Box>
        <Stack>
          <Typography
            variant="caption"
            maxWidth={450}
            textAlign="center"
            color="#8d8787"
          >
            Don't have an account yet? <Link href="#">Sign Up</Link>
          </Typography>
        </Stack>
      </Stack>
    </Box>
  );
};

export default LoginForm;
