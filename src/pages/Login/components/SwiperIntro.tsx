import { Swiper, SwiperSlide } from "swiper/react";
import { Autoplay, Pagination, Navigation } from "swiper/modules";

import "swiper/css";
import "swiper/css/pagination";
import { Stack, Typography } from "@mui/material";
import { listIntro } from "../../../constants";

const SwiperIntro = () => {
  return (
    <Swiper
      pagination={true}
      className="mySwiper"
      loop
      autoplay={{
        delay: 2000,
        disableOnInteraction: false,
      }}
      modules={[Autoplay, Pagination, Navigation]}
    >
      {listIntro.map((item, index) => (
        <SwiperSlide key={index}>
          <Stack
            direction="column"
            justifyContent="center"
            alignItems="center"
            spacing={2}
          >
            <img src={item.img} alt="" />
            <Stack
              direction="column"
              justifyContent="center"
              alignItems="center"
              spacing={1}
            >
              <Typography variant="h5" fontWeight={800} fontSize={30} color="#fff">
                VietNam Smarthub Logistics
              </Typography>
              <Typography
                variant="caption"
                display="block"
                gutterBottom
                maxWidth={300}
                color="#e6dede"
                fontSize={12}
              >
                Giải pháp quản lý điều hành kho bãi. Phát triển nền tảng công
                nghệ hiện - Trí tuệ nhân tạo.
              </Typography>
            </Stack>
          </Stack>
        </SwiperSlide>
      ))}
    </Swiper>
  );
};

export default SwiperIntro;
